package paquete;

/**
 *
 * @author elcap
 */
public class canciones {
    private String Nombre;
    private String Extension;
    private String Artista;
    private String Album;
    private String Genero;
    private String Duracion;
    private String Año;
 
    //Constructor

    /**
     *
     * @param Nombre
     * @param Extension
     * @param Artista
     * @param Album
     * @param Genero
     * @param Duracion
     * @param Año      
     * this.Duracion=Duracion;
     * this.Año=Año;   
    }

     */
    public canciones(String Nombre,String Extension, String Artista,String Album,String Genero,String Duracion,String Año){
       this.Nombre=Nombre;
       this.Extension=Extension;
       this.Artista=Artista;
       this.Album=Album;
       this.Genero=Genero;   
    }
    
    public String getNombre(){
        return Nombre;
    }
    
    public String getGenero(){
        return Genero;
    }
    
    public String getAño(){
        return Año;
    }
    
    public String getDuracion(){
        return Duracion;
    }
    
    public String getArtista(){
        return Artista;
    }
    
    public String getAlbum(){
        return Album;
    }
}
