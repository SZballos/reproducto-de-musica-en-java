package paque;

import basedatos.conectar;
import java.io.File;
import paquete.FMain;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.*;
import java.nio.file.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.RowFilter;
import javax.swing.table.TableRowSorter;



public class Reproductor extends javax.swing.JFrame {
    DefaultTableModel nuevomodelo=new DefaultTableModel();
    
    DefaultTableModel nuevomodelo3=new DefaultTableModel();
    DefaultTableModel nuevomodelo4=new DefaultTableModel();
     
    private String rutaacceso,nom;
    private String rutaacceso2;
    private String id;
    private int control=1;
    private int contadorl=0;
    
    private FileNameExtensionFilter filtro = new FileNameExtensionFilter("Busqueda solo mp3, png, jpg","mp3","png","jpg");
    //private FileNameExtensionFilter filtroimg = new FileNameExtensionFilter("Busqueda solo mp","pgn","jpg");
    
    private TableRowSorter trsFiltro;
    private int repANDpau=1;
    
    private int fila;
    private int columna;
    private int fila2;
    private int columna2;
    private int filalista;
    private int columnalista;
    private int filalista2;
    private int columnalista2;
    private int filaimagen;
    private int columnaimagen;
    String []Lista=new String[10];
    
    String []Infinito;
    String []Infinito2;
    String []Infinito3;
    String []Infinito4;
    String []Infinito5;
    String []Infinito6;
    String []Infinito7;
    String []Infinito8;
    String []Infinito9;
    String []Infinito10;
    
    ImageIcon []Imagenes;
    int contador=0;
    
    private String toString(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public class TablaCanciones extends DefaultTableModel{
        
        public boolean isCellEditable(int row,int colum ){
            return false;
        }
    }
    /**
     * Creates new form Reproductor
     */
    //Claseprin mos=new Claseprin();
    
    
    
    FMain pla=new FMain();
    Maximizar mostra=new Maximizar();
    public Reproductor() {
        initComponents();
        this.setLocationRelativeTo(null);
        Tabla();
        /*for(int i=0; i< Imagenes.length;i++){
        Imagenes[i]=new ImageIcon(getClass().getResource(rutaacceso));
        jLabelMostrarImagen.setIcon(Imagenes[contador]);
        }*/
        
        
        nuevomodelo.addColumn("Nombre");
        nuevomodelo.addColumn("Extensión");
        nuevomodelo.addColumn("Artista");
        nuevomodelo.addColumn("Albúm");
        nuevomodelo.addColumn("Género");
        nuevomodelo.addColumn("Duración");
        nuevomodelo.addColumn("Año");
        nuevomodelo.addColumn("Ruta");
        nuevomodelo.addColumn("Tamaño");
        this.TablaCanciones.setModel(nuevomodelo);
        
        
        //nuevomodelo3.addColumn("Nombre");
        //\nuevomodelo3.addColumn("Ruta");
        //this.TablaLista.setModel(nuevomodelo3);
        
        nuevomodelo4.addColumn("Nombre");
        nuevomodelo4.addColumn("Ruta");
        nuevomodelo4.addColumn("Tamaño");
        nuevomodelo4.addColumn("Extension");
        this.TablaImagenes.setModel(nuevomodelo4);
    }
    
     public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("imagenes/Windows-Media-Player-icon.png"));
        return retValue;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        TextoBuscar = new javax.swing.JTextField();
        BuscarconRuta = new javax.swing.JButton();
        STOP = new javax.swing.JButton();
        Salir = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        TablaCanciones = new javax.swing.JTable();
        jButton_minimizar = new javax.swing.JButton();
        BuscarManual = new javax.swing.JButton();
        ReproduccionActual = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        TablaListadeCanciones = new javax.swing.JTable();
        jTextField1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        EliminarLista = new javax.swing.JButton();
        jTexListas = new javax.swing.JTextField();
        AgregarLista = new javax.swing.JButton();
        AgregarALista = new javax.swing.JButton();
        Mover = new javax.swing.JButton();
        Eliminar = new javax.swing.JButton();
        Siguiente = new javax.swing.JButton();
        Anterior = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        TablaLista = new javax.swing.JTable();
        QuitardeLista = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        AbrirLista = new javax.swing.JButton();
        PlayPausa = new javax.swing.JButton();
        jLabelMostrarImagen = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        TablaImagenes = new javax.swing.JTable();
        jTextField3 = new javax.swing.JTextField();
        Abrir = new javax.swing.JButton();
        EliminarImagen = new javax.swing.JButton();
        MoverImagenes = new javax.swing.JButton();
        ImagenSiguiente = new javax.swing.JButton();
        ImagenAnterior = new javax.swing.JButton();
        BotonMax = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        FiltroItem = new javax.swing.JComboBox<>();
        BusquedaCanciones = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextId = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextL = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Reproductor de Música");
        setIconImage(getIconImage());
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Buscar");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 110, 50, 30));

        TextoBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextoBuscarActionPerformed(evt);
            }
        });
        getContentPane().add(TextoBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 70, 440, 30));

        BuscarconRuta.setText("Buscar");
        BuscarconRuta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                BuscarconRutaMouseReleased(evt);
            }
        });
        BuscarconRuta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarconRutaActionPerformed(evt);
            }
        });
        getContentPane().add(BuscarconRuta, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 70, 120, 30));

        STOP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Stop_2001885.png"))); // NOI18N
        STOP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                STOPMouseReleased(evt);
            }
        });
        STOP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                STOPActionPerformed(evt);
            }
        });
        getContentPane().add(STOP, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 20, 40, 40));

        Salir.setBackground(new java.awt.Color(255, 51, 51));
        Salir.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        Salir.setForeground(new java.awt.Color(255, 255, 255));
        Salir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Cross_2001870 (1).png"))); // NOI18N
        Salir.setText("SALIR");
        Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirActionPerformed(evt);
            }
        });
        getContentPane().add(Salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 570, 110, 40));

        TablaCanciones = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        TablaCanciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9"
            }
        ));
        TablaCanciones.getTableHeader().setReorderingAllowed(false);
        TablaCanciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                TablaCancionesMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(TablaCanciones);

        getContentPane().add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 150, 630, 120));

        jButton_minimizar.setBackground(new java.awt.Color(51, 204, 255));
        jButton_minimizar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton_minimizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Minus_2001871.png"))); // NOI18N
        jButton_minimizar.setText("Minimizar");
        jButton_minimizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_minimizarActionPerformed(evt);
            }
        });
        getContentPane().add(jButton_minimizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 570, 140, 40));

        BuscarManual.setText("Ir a Buscar");
        BuscarManual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarManualActionPerformed(evt);
            }
        });
        getContentPane().add(BuscarManual, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 110, 120, 30));

        ReproduccionActual.setEditable(false);
        ReproduccionActual.setFont(new java.awt.Font("Courier New", 1, 18)); // NOI18N
        ReproduccionActual.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReproduccionActualActionPerformed(evt);
            }
        });
        getContentPane().add(ReproduccionActual, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 30, 440, 30));

        TablaListadeCanciones = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        TablaListadeCanciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Lista nombre"
            }
        ));
        TablaListadeCanciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                TablaListadeCancionesMouseReleased(evt);
            }
        });
        TablaListadeCanciones.addVetoableChangeListener(new java.beans.VetoableChangeListener() {
            public void vetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {
                TablaListadeCancionesVetoableChange(evt);
            }
        });
        jScrollPane1.setViewportView(TablaListadeCanciones);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 140, 180, 130));

        jTextField1.setEditable(false);
        jTextField1.setText("Imagenes");
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 510, 160, 20));

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("REPRODUCIENDO:");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 30, 190, 30));

        EliminarLista.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        EliminarLista.setText("Eliminar Lista");
        EliminarLista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarListaActionPerformed(evt);
            }
        });
        getContentPane().add(EliminarLista, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 100, 110, 20));

        jTexListas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTexListasActionPerformed(evt);
            }
        });
        getContentPane().add(jTexListas, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 120, 180, 20));

        AgregarLista.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        AgregarLista.setText("Agregar Lista");
        AgregarLista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarListaActionPerformed(evt);
            }
        });
        getContentPane().add(AgregarLista, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 80, 110, -1));

        AgregarALista.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Menu_2001884.png"))); // NOI18N
        AgregarALista.setText("Agregar a lista");
        AgregarALista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AgregarAListaActionPerformed(evt);
            }
        });
        getContentPane().add(AgregarALista, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 150, 160, 40));

        Mover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_More_2001881.png"))); // NOI18N
        Mover.setText("Mover              ");
        Mover.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MoverActionPerformed(evt);
            }
        });
        getContentPane().add(Mover, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 230, 160, -1));

        Eliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Cross_2001870 (1).png"))); // NOI18N
        Eliminar.setText("Eliminar           ");
        Eliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EliminarActionPerformed(evt);
            }
        });
        getContentPane().add(Eliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 190, 160, -1));

        Siguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Fast_Forward_2001867.png"))); // NOI18N
        Siguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SiguienteActionPerformed(evt);
            }
        });
        getContentPane().add(Siguiente, new org.netbeans.lib.awtextra.AbsoluteConstraints(970, 20, 40, -1));

        Anterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Rewind_2001873 (1).png"))); // NOI18N
        Anterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AnteriorActionPerformed(evt);
            }
        });
        getContentPane().add(Anterior, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 20, 40, 40));

        TablaLista = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        TablaLista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        TablaLista.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                TablaListaMouseReleased(evt);
            }
        });
        jScrollPane4.setViewportView(TablaLista);

        getContentPane().add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 340, 340, 150));

        QuitardeLista.setText("Quitar de lista");
        QuitardeLista.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                QuitardeListaMouseReleased(evt);
            }
        });
        QuitardeLista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QuitardeListaActionPerformed(evt);
            }
        });
        getContentPane().add(QuitardeLista, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 300, -1, 30));

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Ingrese nombre");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 120, 130, 30));

        AbrirLista.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        AbrirLista.setText("Abrir Lista");
        AbrirLista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AbrirListaActionPerformed(evt);
            }
        });
        getContentPane().add(AbrirLista, new org.netbeans.lib.awtextra.AbsoluteConstraints(1070, 270, 100, -1));

        PlayPausa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/if_Forward_Skip_2001880.png"))); // NOI18N
        PlayPausa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PlayPausaActionPerformed(evt);
            }
        });
        getContentPane().add(PlayPausa, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 20, 40, -1));

        jLabelMostrarImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMostrarImagen.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 5, true));
        getContentPane().add(jLabelMostrarImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 290, 380, 210));

        TablaImagenes = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        TablaImagenes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        TablaImagenes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                TablaImagenesMouseReleased(evt);
            }
        });
        jScrollPane5.setViewportView(TablaImagenes);

        getContentPane().add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 530, 340, 90));

        jTextField3.setEditable(false);
        jTextField3.setText("Canciones de lista");
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });
        getContentPane().add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 310, 160, 20));

        Abrir.setText("Abrir Imagenes");
        Abrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AbrirActionPerformed(evt);
            }
        });
        getContentPane().add(Abrir, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 630, -1, -1));

        EliminarImagen.setText("Eliminar Imagen");
        getContentPane().add(EliminarImagen, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 630, -1, -1));

        MoverImagenes.setText("Mover Imagen");
        MoverImagenes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MoverImagenesActionPerformed(evt);
            }
        });
        getContentPane().add(MoverImagenes, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 630, -1, -1));

        ImagenSiguiente.setText("Siguiente");
        ImagenSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImagenSiguienteActionPerformed(evt);
            }
        });
        getContentPane().add(ImagenSiguiente, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 510, -1, -1));

        ImagenAnterior.setText("Anterior");
        ImagenAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImagenAnteriorActionPerformed(evt);
            }
        });
        getContentPane().add(ImagenAnterior, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 510, -1, -1));

        BotonMax.setText("Maximizar");
        BotonMax.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonMaxActionPerformed(evt);
            }
        });
        getContentPane().add(BotonMax, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 510, -1, -1));

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Ingrese Ruta");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, 130, 30));

        FiltroItem.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre", "Artistas", "Album", "Genero", "Tamaño", "Año", "Duración" }));
        FiltroItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FiltroItemActionPerformed(evt);
            }
        });
        getContentPane().add(FiltroItem, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 110, -1, -1));

        BusquedaCanciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BusquedaCancionesActionPerformed(evt);
            }
        });
        BusquedaCanciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                BusquedaCancionesKeyTyped(evt);
            }
        });
        getContentPane().add(BusquedaCanciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 110, 290, 20));

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Nombre Lista");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 280, 110, 20));

        jTextId.setEditable(false);
        getContentPane().add(jTextId, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 280, 60, -1));

        jLabel7.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("ID");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 280, 20, 20));

        jTextL.setEditable(false);
        getContentPane().add(jTextL, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 280, 150, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/apollo__god_of_music.png"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 51, 51), 12, true));
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1210, 670));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //public void inf(){
        
    //}
    
    
    private void STOPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_STOPActionPerformed
        
    }//GEN-LAST:event_STOPActionPerformed
        
    private void STOPMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_STOPMouseReleased
        //BOTON PAUSA
        pla.stop();
        repANDpau=1;
    }//GEN-LAST:event_STOPMouseReleased

    private void BuscarconRutaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BuscarconRutaMouseReleased
        //Boton buscar pero con ruta
        
        String ruta;
        //ruta=TextoBuscar.getText();
        ruta=TextoBuscar.getText();
        
        System.out.println(ruta);
        if(ruta!=null&&!ruta.equals("")){   
        listadatos(ruta);    
        }else{
        JOptionPane.showMessageDialog(null, "Error en la ruta, porfavor verifique la ruta", "RUTA VACÍA", 0);    
        }        
    }//GEN-LAST:event_BuscarconRutaMouseReleased

    private void TextoBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextoBuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TextoBuscarActionPerformed

    private void SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_SalirActionPerformed

    private void BuscarconRutaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarconRutaActionPerformed
        JOptionPane.showMessageDialog(null, "Se estan cargando los archivo", "Espere un momento", 2);
    }//GEN-LAST:event_BuscarconRutaActionPerformed

    private void jButton_minimizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_minimizarActionPerformed
        // Minimizar   
        this.setExtendedState(ICONIFIED);
    }//GEN-LAST:event_jButton_minimizarActionPerformed

    private void BuscarManualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarManualActionPerformed
        
        
        
        listadatos2();
    }//GEN-LAST:event_BuscarManualActionPerformed

    private void ReproduccionActualActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReproduccionActualActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ReproduccionActualActionPerformed

    private void mover(){
        if(rutaacceso!=null){
            try {
                Path orige=Paths.get(rutaacceso);
                Path destino=Paths.get("C:\\Users\\elcap\\Desktop\\Nueva carpeta");
                
                Files.move(orige, destino.resolve(orige.getFileName()));
            } catch (IOException ex) {
                System.out.println("Verifique el archivo");

                //Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        
    }
    
    
    private void TablaCancionesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaCancionesMouseReleased
        //Tabla reproducir
        
        fila = TablaCanciones.rowAtPoint(evt.getPoint());
        columna = 7;//TablaCanciones.columnAtPoint(evt.getPoint());
        if ((fila > -1) && (columna > -1))
            rutaacceso=(String)TablaCanciones.getValueAt(fila, columna);
            //TablaCanciones.setValueAt(evt, fila, columna);
            System.out.println(rutaacceso);
            JOptionPane.showMessageDialog(null, "                  Se ha seleccionado la Canción\nSi esta reproduciendo alguna canción esta se pausara\n"
                    + "               Presione play para continuar", "INFORMACION", 1);
            pla.stop();
            repANDpau=1;
            ReproduccionActual.setText("");
            
            
    }//GEN-LAST:event_TablaCancionesMouseReleased

    
    
    void prueba(){
        //String []list=new String[10]; 
        //list[0]="Lista 1";
       Infinito[contadorl]="C:\\Ruta";
        
    }
    
    private void Eliminar(){
        
        //JFileChooser Reciclaje =new JFileChooser();
        if(rutaacceso!=null){
            File Eliminartabla=new File(rutaacceso);
            
            int selec=JOptionPane.showOptionDialog(null,"Seguro que desea eliminar el archivo?" , "Eliminar archivo", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,null, null, null);
                if(Eliminartabla!=null){
                    
                    if(selec==0&&rutaacceso.endsWith(".mp3")){
                    Eliminartabla.delete();
                    nuevomodelo.removeRow(TablaCanciones.getSelectedRow());
                    }else{
                        if(selec==0&&(rutaacceso.endsWith(".png")||rutaacceso.endsWith(".jpg"))){
                        Eliminartabla.delete();
                        nuevomodelo4.removeRow(TablaImagenes.getSelectedRow());    
                        }
                        
                    }
                }
                System.out.println("OPCION: "+selec);
        }
            
    }
    
        /*public void rutanuevamover(String m,int f, int c ){
       System.out.println("rutanuevamover: "+m+" Fila: "+f+ " Columna: "+c);
       TablaCanciones.setValueAt("m",f, c);
    }*/
    
    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jTexListasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTexListasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTexListasActionPerformed

    private void PlayPausaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PlayPausaActionPerformed
        //
        if(rutaacceso.endsWith(".mp3")){
            if(repANDpau==1){
                File f=new File(rutaacceso); 
                String n=f.getName();
                ReproduccionActual.setText(n);
                pla.localizaciondirectorio=rutaacceso;
                pla.Reanudar();
                System.out.println(repANDpau);
                repANDpau=0;
            }else{
                pla.Pausa();
                System.out.println(repANDpau);
                repANDpau=1;          
            }  
        }
    }//GEN-LAST:event_PlayPausaActionPerformed

    private void EliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarActionPerformed
        // Eliminar
        ElimValid();
        
    }//GEN-LAST:event_EliminarActionPerformed
    void ElimValid(){
        if(rutaacceso!=null){
        Eliminar();    
        }else{
            System.out.println("Nop..... ");
        }
    }
    private void MoverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MoverActionPerformed
        //String Mover=rutaacceso;
        //mover();
        //Reproductor Mostrar2= new Reproductor();
        //Mostrar2.setVisible(false);
        if(rutaacceso.endsWith(".mp3")){
        moverArch();    
        }
        
        
        //Mostrar.mover(rutaacceso);    
    }//GEN-LAST:event_MoverActionPerformed

    public void moverArch(){
       if(rutaacceso!=null){
        NewJFrameMover Mostrar= new NewJFrameMover();
        Mostrar.setVisible(true);
        Mostrar.setRutaop(rutaacceso);
        Mostrar.setFila(fila);
        Mostrar.mostrar();
        System.out.println(rutaacceso);  
        Mostrar.BotonMover();
        }else{
        System.out.println("Nop..... ");
        } 
    }
    
    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void TablaImagenesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaImagenesMouseReleased
        fila = TablaImagenes.rowAtPoint(evt.getPoint());
        columna = 1;//TablaCanciones.columnAtPoint(evt.getPoint());
        //if ((fila > -1) && (columna > -1))
            System.out.println("Fila: "+fila+"    Columna: "+columna);
            rutaacceso=(String)TablaImagenes.getValueAt(fila, columna);
            System.out.println(rutaacceso);
    }//GEN-LAST:event_TablaImagenesMouseReleased

    private void verImagen(){
        
    } 
    
    
    private void TablaListadeCancionesMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaListadeCancionesMouseReleased
        // Tabla lista
        //String []lista;
        fila= TablaListadeCanciones.rowAtPoint(evt.getPoint());
        columna= 0;//TablaCanciones.columnAtPoint(evt.getPoint());
        //if ((fila > -1) && (columna > -1))
            
        id=(String)TablaListadeCanciones.getValueAt(fila, columna);
        columna= 1;
        nom=(String)TablaListadeCanciones.getValueAt(fila, columna);
        System.out.println(nom);
    }//GEN-LAST:event_TablaListadeCancionesMouseReleased

    private void ImagenSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ImagenSiguienteActionPerformed
        // Siguiente Imagenes
        //if(contador< Imagenes.length){
          //  contador++;
        //}
        
        if((rutaacceso.endsWith(".jpg")||rutaacceso.endsWith(".png")||rutaacceso.endsWith(".PNG"))&&fila<TablaImagenes.getRowCount()-1){
        fila++;
        
        rutaacceso=(String)TablaImagenes.getValueAt(fila, columna);
        //pla.localizaciondirectorio=rutaacceso;
        //File R=new File(rutaacceso);
        //pla.Reanudar();
        ver();
        //ReproduccionActual.setText(R.getName());
           
        }
    }//GEN-LAST:event_ImagenSiguienteActionPerformed

    private void ImagenAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ImagenAnteriorActionPerformed
        //  Atras Imagen
        //if(contador > Imagenes.length){
          //  contador--;
        //}
        if((rutaacceso.endsWith(".jpg")||rutaacceso.endsWith(".png")||rutaacceso.endsWith(".PNG"))&&fila>0){
        fila--;
        
        rutaacceso=(String)TablaImagenes.getValueAt(fila, columna);
        ver(); 
        }
    }//GEN-LAST:event_ImagenAnteriorActionPerformed

    private void AbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AbrirActionPerformed
        //Ver Imagen
        //mostra.setRutaope(rutaacceso2);
        ver();
        //rutaacceso2="";
    }//GEN-LAST:event_AbrirActionPerformed

    private void BotonMaxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonMaxActionPerformed
        //Maximizar bóton
        
        mostra.setVisible(true);
        //mostra.setRutaope(rutaacceso2);
        mostra.verImagen(rutaacceso);
        
    }//GEN-LAST:event_BotonMaxActionPerformed

    private void MoverImagenesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MoverImagenesActionPerformed
        // Mover Imagenes
       if(rutaacceso.endsWith(".jpg")||rutaacceso.endsWith(".png")){
       moverArch();
       }
    }//GEN-LAST:event_MoverImagenesActionPerformed

    conectar cc=new conectar();
    Connection cn= cc.conexion();
    
    private void AgregarListaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarListaActionPerformed
        if(!(jTexListas.getText().isEmpty())&&jTexListas!=null){
        agregarLista();
        JOptionPane.showMessageDialog(null, "Lista Agregada Correctamente");
        }else{
        JOptionPane.showMessageDialog(null, "Verifique el nombre de la Lista");
        }
    }//GEN-LAST:event_AgregarListaActionPerformed

    void Tabla(){
    DefaultTableModel nuevomodelo2=new DefaultTableModel();
    nuevomodelo2.addColumn("ID");
    nuevomodelo2.addColumn("Listas");
    this.TablaListadeCanciones.setModel(nuevomodelo2);
        
    String []datos =new String[2];
    
    
        try {
            Statement st=cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT *FROM playlist");
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                nuevomodelo2.addRow(datos);
            }
        } catch (Exception e) {
        }
    }
    
    
    void agregarLista(){
    
        try {
            
            PreparedStatement pst=cn.prepareStatement("INSERT INTO playlist(Nombre) VALUES (?)");            
            pst.setString(1, jTexListas.getText());
          
            pst.executeUpdate();
            
            Tabla();
            Clean();
        } catch (SQLException ex) {
            Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void Clean(){
    
    }
    
    private void EliminarListaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EliminarListaActionPerformed
        //Eliminar de Lista
        //nuevomodelo2.removeRow(TablaListadeCanciones.getSelectedRow());
        //Tabla();
        
        if(!(id.isEmpty())&&id!=null){
        eliminarLista(id);
        id="";
        Tabla();
        verTablaLista();
        jTextId.setText("");
        jTextL.setText("");
        }
        
    }//GEN-LAST:event_EliminarListaActionPerformed

    void eliminarLista(String id){
        
        try {
            //PreparedStatement ps2=cn.prepareStatement("DELETE FROM canciones WHERE idLista='"+id+"'");
            PreparedStatement ps=cn.prepareStatement("DELETE FROM playlist WHERE IdPlayList='"+id+"'");
            //ps2.executeUpdate();
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void SiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SiguienteActionPerformed
        //Botón Siguiente
        if(rutaacceso.endsWith(".mp3")&&fila<TablaCanciones.getRowCount()-1){
        fila++;
        pla.stop();
        rutaacceso=(String)TablaCanciones.getValueAt(fila, columna);
        pla.localizaciondirectorio=rutaacceso;
        File R=new File(rutaacceso);
        pla.Reanudar();
        ReproduccionActual.setText(R.getName());
        repANDpau=0;    
        }
    }//GEN-LAST:event_SiguienteActionPerformed

    private void AnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AnteriorActionPerformed
        // Canción Anterior
        if(rutaacceso.endsWith(".mp3")&&fila>0){
        fila--;
        pla.stop();
        rutaacceso=(String)TablaCanciones.getValueAt(fila, columna);
        pla.localizaciondirectorio=rutaacceso;
        File R=new File(rutaacceso);
        pla.Reanudar();
        ReproduccionActual.setText(R.getName());
        repANDpau=0;    
        }
    }//GEN-LAST:event_AnteriorActionPerformed

    private void BusquedaCancionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BusquedaCancionesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BusquedaCancionesActionPerformed

    private void FiltroItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FiltroItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_FiltroItemActionPerformed

    private void BusquedaCancionesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_BusquedaCancionesKeyTyped
        // Filtro
        BusquedaCanciones.addKeyListener(new KeyAdapter(){
            public void keyReleased(final KeyEvent e){
                String cadena=(BusquedaCanciones.getText());
                BusquedaCanciones.setText(cadena);
                repaint();
                filtro();
            }
        });
        
        trsFiltro=new TableRowSorter(TablaCanciones.getModel());
        TablaCanciones.setRowSorter(trsFiltro);
        
    }//GEN-LAST:event_BusquedaCancionesKeyTyped

    private void AbrirListaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AbrirListaActionPerformed
        jTextId.setText(id);
        jTextL.setText(nom);
        abrirLista();
    }//GEN-LAST:event_AbrirListaActionPerformed

    void abrirLista(){
        System.out.println(jTextId.getText());
        verTablaLista();
    }
    
    void verTablaLista(){
    DefaultTableModel nuevomodelo2=new DefaultTableModel();
    nuevomodelo2.addColumn("ID");
    nuevomodelo2.addColumn("Nombre");
    nuevomodelo2.addColumn("Ruta");
    //nuevomodelo2.addColumn("idLista");
    this.TablaLista.setModel(nuevomodelo2);
        
    String []datos =new String[3];
    
       // int ids=Integer.parseInt(jTextId.getText());
        try {
            Statement st=cn.createStatement();
            ResultSet rs = st.executeQuery("SELECT id,Nombre,Ruta FROM canciones WHERE idLista='"+Integer.parseInt(jTextId.getText())+"'");
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                //datos[2]=rs.getString(4);
                nuevomodelo2.addRow(datos);
            }
        } catch (Exception e) {
        }
    }
    
    
    private void AgregarAListaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AgregarAListaActionPerformed
        // Agregar a lista
       if(!(jTextId.getText().isEmpty())&&jTextId!=null){
       agregarCanciones();
       JOptionPane.showMessageDialog(null, "Agregada correctamente");
       }else{
       JOptionPane.showMessageDialog(null, "Seleccione una lista antes y presione click en el boton \"Abrir lista\" ");
       }
        
        //Infinito[contador]=
    }//GEN-LAST:event_AgregarAListaActionPerformed

    void agregarCanciones(){
        String []R=new String[2];
        R[1]=(String)TablaCanciones.getValueAt(fila, 7);
        R[0]=(String)TablaCanciones.getValueAt(fila, 0);
        agregarTablaLista(R[0],R[1]);
    }
    
    void agregarTablaLista(String nombre,String ruta){   
        try {
            
            PreparedStatement pst=cn.prepareStatement("INSERT INTO canciones(Nombre,Ruta,idLista) VALUES (?,?,?)");            
            pst.setString(1, nombre);
            pst.setString(2, ruta);
            pst.setInt(3, Integer.parseInt(jTextId.getText()));
            pst.executeUpdate();
            
            //Tabla();
            //Clean();
            verTablaLista();
        } catch (SQLException ex) {
            Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void TablaListaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaListaMouseReleased
        // Tabla
        fila = TablaLista.rowAtPoint(evt.getPoint());
        columna = 2;//TablaCanciones.columnAtPoint(evt.getPoint());
        //if ((fila > -1) && (columna > -1))
            System.out.println("Fila: "+fila+"    Columna: "+columna);
            rutaacceso=(String)TablaLista.getValueAt(fila, columna);
            System.out.println(rutaacceso);
    }//GEN-LAST:event_TablaListaMouseReleased

    private void QuitardeListaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QuitardeListaActionPerformed
       // nuevomodelo3.removeRow(TablaLista.getSelectedRow());
    }//GEN-LAST:event_QuitardeListaActionPerformed

    void quitarLista(String id){
        
        try {
            PreparedStatement ps=cn.prepareStatement("DELETE FROM canciones WHERE id='"+id+"'");
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Reproductor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void TablaListadeCancionesVetoableChange(java.beans.PropertyChangeEvent evt)throws java.beans.PropertyVetoException {//GEN-FIRST:event_TablaListadeCancionesVetoableChange
        // TODO add your handling code here:
    }//GEN-LAST:event_TablaListadeCancionesVetoableChange

    private void QuitardeListaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_QuitardeListaMouseReleased
        String id;
        id=(String)TablaLista.getValueAt(fila2, 0);
        quitarLista(id);
        verTablaLista();
    }//GEN-LAST:event_QuitardeListaMouseReleased
   
    
    public void recorrer(){
        
    }
    private void filtro(){
        int columnaABuscar=0;
        if(FiltroItem.getSelectedItem()=="Nombre"){
           columnaABuscar=0;
        }
        if(FiltroItem.getSelectedItem()=="Artistas"){
           columnaABuscar=2;
        }
        if(FiltroItem.getSelectedItem()=="Album"){
            columnaABuscar=3;
        }
        if(FiltroItem.getSelectedItem()=="Genero"){
            columnaABuscar=4;
        }
        if(FiltroItem.getSelectedItem()=="Tamaño"){
            columnaABuscar=8;
        }
        if(FiltroItem.getSelectedItem()=="Año"){
            columnaABuscar=6;
        }
        if(FiltroItem.getSelectedItem()=="Duración "){
            columnaABuscar=5;
        }
        trsFiltro.setRowFilter(RowFilter.regexFilter(BusquedaCanciones.getText(), columnaABuscar));
    }
    
    
    private void ver(){
        if(rutaacceso!=""){
        ImageIcon MostrarImagen=new ImageIcon(rutaacceso);
        //int ancho=MostrarImagen.getIconWidth();
        //int alto=MostrarImagen.getIconHeight();
        
        //this.jLabelMostrarImagen.setSize(ancho, alto);
        this.jLabelMostrarImagen.setIcon(MostrarImagen); 
        System.out.println("Mostrar ruta: "+rutaacceso);
        System.out.println("HOLLLLLAAALALALALALALAALAL");
        }
        
    }
    
    public void listadatos(String acceso){
        File[] listadeArchivo;
        //File[] listadeArchivoImagenes;
        String []list=new String[9];
        //String []list2=new String[4];
        //String []ListImg = null;
        
        String path = acceso;
        String archivo;
        File folder = new File(path);
        listadeArchivo = folder.listFiles();
        
        if(path.endsWith(".mp3")){
        pla.stop();
        rutaacceso=path;
        pla.localizaciondirectorio=rutaacceso;
        pla.Reanudar();
        repANDpau=0;
        listaSoloUnaCancion(folder);
        
        control++;
        }else{
            
            if(path.endsWith(".jpg")||path.endsWith(".png")||path.endsWith(".PNG")){
            //listaSoloUnaImagen(folder);
            list[0]=folder.getName();
            list[1]=folder.getAbsolutePath();
            list[2]=(Long.toString(folder.length()))+" bite";
            if(path.endsWith(".jpg")){
                list[3]="jpg";    
            }else{    
                list[3]="png";
                }
            nuevomodelo4.addRow(list);    
            control++;    
            }else{
        
            for (int i=0; i< listadeArchivo.length; i++) {
            
                if (listadeArchivo[i].isFile()) {
                    archivo = listadeArchivo[i].getName();
                    if(archivo.endsWith(".mp3")){
                        try {
                            AudioFile ff = AudioFileIO.read(listadeArchivo[i].getAbsoluteFile());
                            Tag tag=ff.getTag();
                            AudioHeader AudioHeader = ff.getAudioHeader();
                        
                            //Nombre
                            list[0]=listadeArchivo[i].getName();
                            //Formato
                            list[1]="mp3";
                            //Artista
                            if(tag.getFirst(FieldKey.ARTIST)!=""){
                                list[2]=tag.getFirst(FieldKey.ARTIST);
                            }else{    
                                list[2]="No Disponible";
                            }
                            //Album
                            if(tag.getFirst(FieldKey.ALBUM)!=""){
                                list[3]=tag.getFirst(FieldKey.ALBUM);
                            }else{
                                list[3]="No Disponible";
                            }
                            //Genero
                            if(tag.getFirst(FieldKey.GENRE)!=""){
                                list[4]=tag.getFirst(FieldKey.GENRE);
                            }else{
                                list[4]="No Disponible";
                            }
                            //Tiempo de la canción++
                            list[5]=String.format("%.2f",ff.getAudioHeader().getTrackLength()/60.0);
                            //Año de la Canción
                            if(tag.getFirst(FieldKey.YEAR)!=""){
                                list[6]=tag.getFirst(FieldKey.YEAR);
                            }else{
                                list[6]="No Disponible";
                            }
                            //Ruta
                            list[7]=listadeArchivo[i].getAbsolutePath();
                            //Tamaño de la Canción
                            list[8]=(Long.toString(listadeArchivo[i].length()/1024))+" KB";
                            nuevomodelo.addRow(list);
                        
                        
                        
                            System.out.println(archivo);
                            } catch (CannotReadException | IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException ex) {
                        
                            }
                        control++;
                    }else{
                        if((archivo.endsWith(".jpg")||archivo.endsWith(".png"))&&(listadeArchivo[i].canWrite())){
                        //
                            //ListImg[contador]=listadeArchivo[i].getAbsolutePath();
                            list[0]=listadeArchivo[i].getName();
                            list[1]=listadeArchivo[i].getAbsolutePath();
                            list[2]=(Long.toString(listadeArchivo[i].length()))+" bite";
                            if(archivo.endsWith(".jpg")){
                                list[3]="jpg";    
                            }else{    
                                list[3]="png";
                            }
                            nuevomodelo4.addRow(list);
                            System.out.println(archivo);
                            control++;
                        }
                    }
                }else{
                    listadatos(listadeArchivo[i].getAbsolutePath());
                    
                }    
            }
        } 
    }
}

    public void listaSoloUnaCancion(File folder){
        String []list=new String[9];
        File fol=folder;
        AudioFile ff;
        try {
            ReproduccionActual.setText(fol.getName());
            ff = AudioFileIO.read(fol.getAbsoluteFile());
            Tag tag=ff.getTag();
            AudioHeader AudioHeader = ff.getAudioHeader();
            list[0]=fol.getName();
            //Formato
            list[1]="mp3";
            //Artista
            if(tag.getFirst(FieldKey.ARTIST)!=""){
                list[2]=tag.getFirst(FieldKey.ARTIST);
            }else{    
                list[2]="No Disponible";
                }
            //Album
            if(tag.getFirst(FieldKey.ALBUM)!=""){
                list[3]=tag.getFirst(FieldKey.ALBUM);
            }else{
                list[3]="No Disponible";
                }
            //Genero
            if(tag.getFirst(FieldKey.GENRE)!=""){
                list[4]=tag.getFirst(FieldKey.GENRE);
            }else{
                list[4]="No Disponible";
                }
            //Tiempo de la canción++
            list[5]=String.format("%.2f",ff.getAudioHeader().getTrackLength()/60.0);
            //Año de la Canción
            if(tag.getFirst(FieldKey.YEAR)!=""){
                list[6]=tag.getFirst(FieldKey.YEAR);
            }else{
                list[6]="No Disponible";
            }
            //Ruta
            list[7]=fol.getAbsolutePath();
            //Tamaño de la Canción
            list[8]=(Long.toString(fol.length()/1024))+" KB";
            nuevomodelo.addRow(list);
            
        } catch (CannotReadException | IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException ex) {
            
        }               
    } 
    
    public void listadatos2(){
        File[] listadeArchivo;
        String []list=new String[9];
        
        JFileChooser FicheroAbrir=new JFileChooser();
        FicheroAbrir.setFileFilter(filtro);
        FicheroAbrir.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        if(FicheroAbrir.showOpenDialog(this)==JFileChooser.APPROVE_OPTION){
            File Seleccion=FicheroAbrir.getSelectedFile();
            listadatos(Seleccion.getAbsolutePath());
        }
    }    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Reproductor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Reproductor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Reproductor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Reproductor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Reproductor().setVisible(true);
            }
        });
        
        
        //if(pla.getCanfin()!=false){
            
        //} else {
        //}
    }
    
    public String getRutaacceso(){
        return rutaacceso;
    }
    
    public void setRutaacceso(String rutaacceso){
        this.rutaacceso=rutaacceso;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Abrir;
    private javax.swing.JButton AbrirLista;
    private javax.swing.JButton AgregarALista;
    private javax.swing.JButton AgregarLista;
    private javax.swing.JButton Anterior;
    private javax.swing.JButton BotonMax;
    private javax.swing.JButton BuscarManual;
    private javax.swing.JButton BuscarconRuta;
    private javax.swing.JTextField BusquedaCanciones;
    private javax.swing.JButton Eliminar;
    private javax.swing.JButton EliminarImagen;
    private javax.swing.JButton EliminarLista;
    private javax.swing.JComboBox<String> FiltroItem;
    private javax.swing.JButton ImagenAnterior;
    private javax.swing.JButton ImagenSiguiente;
    private javax.swing.JButton Mover;
    private javax.swing.JButton MoverImagenes;
    private javax.swing.JButton PlayPausa;
    private javax.swing.JButton QuitardeLista;
    private javax.swing.JTextField ReproduccionActual;
    private javax.swing.JButton STOP;
    private javax.swing.JButton Salir;
    private javax.swing.JButton Siguiente;
    private javax.swing.JTable TablaCanciones;
    private javax.swing.JTable TablaImagenes;
    private javax.swing.JTable TablaLista;
    private javax.swing.JTable TablaListadeCanciones;
    private javax.swing.JTextField TextoBuscar;
    private javax.swing.JButton jButton_minimizar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabelMostrarImagen;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextField jTexListas;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextId;
    private javax.swing.JTextField jTextL;
    // End of variables declaration//GEN-END:variables
}